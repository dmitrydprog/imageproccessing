﻿using System;
using System.Windows.Forms;

namespace ImageProccessingForm
{
	public partial class SmoothFormSetting : Form
	{
		#region Public Constructors

		public SmoothFormSetting()
		{
			InitializeComponent();
		}

		#endregion Public Constructors

		#region Public Properties

		public int SmoothLevel { get; private set; }

		#endregion Public Properties

		#region Private Methods

		private void button1_Click(object sender, EventArgs e)
		{
			this.SmoothLevel = (int)numericUpDown1.Value;
		}

		#endregion Private Methods
	}
}