﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ImageProccessing;

namespace ImageProccessingForm
{
	public partial class MainForm : Form
	{
		#region Public Constructors

		public MainForm()
		{
			InitializeComponent();
		}

		#endregion Public Constructors

		#region Private Methods

		private void brnSobel_Click(object sender, EventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
			this.loadingLabel.Text = "Применение эффекта...";

			pictureBox.Image = ip.Sobel().Image;
			this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

			this.loadingLabel.Text = "Завершено";
			pictureBox.Refresh();
		}

		private void btnBinarizate_Click(object sender, EventArgs e)
		{
			BinarizateFormSetting bfs = new BinarizateFormSetting();

			if (bfs.ShowDialog() == DialogResult.OK)
			{
				ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
				this.loadingLabel.Text = "Применение эффекта...";

				ip.Binarizate(bfs.LimValue);
				this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

				this.loadingLabel.Text = "Завершено";
				pictureBox.Refresh();
			}
		}

		private void btnGamma_Click(object sender, EventArgs e)
		{
			GammaFormSettings gfs = new GammaFormSettings();

			if (gfs.ShowDialog() == DialogResult.OK)
			{
				ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
				this.loadingLabel.Text = "Применение эффекта...";

				ip.SetGamma(gfs.Gamma);
				this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

				this.loadingLabel.Text = "Завершено";
				pictureBox.Refresh();
			}
		}

		private void btnGrayscale_Click(object sender, EventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
			this.loadingLabel.Text = "Применение эффекта...";

			ip.ConvertToGrayScale();
			this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

			this.loadingLabel.Text = "Завершено";
			pictureBox.Refresh();
		}

		private void btnKirsh_Click(object sender, EventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
			this.loadingLabel.Text = "Применение эффекта...";

			pictureBox.Image = ip.Kirsh().Image;
			this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

			this.loadingLabel.Text = "Завершено";
			pictureBox.Refresh();
		}

		private void btnLaplas_Click(object sender, EventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
			this.loadingLabel.Text = "Применение эффекта...";

			pictureBox.Image = ip.Laplas().Image;
			this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

			this.loadingLabel.Text = "Завершено";
			pictureBox.Refresh();
		}

		private void btnNaegative_Click(object sender, EventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
			this.loadingLabel.Text = "Применение эффекта...";

			ip.Negative();
			this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

			this.loadingLabel.Text = "Завершено";
			pictureBox.Refresh();
		}

		private void btnOpenFileDialog_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();

			if (ofd.ShowDialog() == DialogResult.OK)
			{
				pictureBox.Image = Bitmap.FromFile(ofd.FileName);
			}
		}

		private void btnRoberts_Click(object sender, EventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
			this.loadingLabel.Text = "Применение эффекта...";

			pictureBox.Image = ip.Roberts().Image;
			this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

			this.loadingLabel.Text = "Завершено";
			pictureBox.Refresh();
		}

		private void btnSmooth_Click(object sender, EventArgs e)
		{
			SmoothFormSetting sfs = new SmoothFormSetting();

			if (sfs.ShowDialog() == DialogResult.OK)
			{
				ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);
				this.loadingLabel.Text = "Применение эффекта...";

				ip.Smooth(sfs.SmoothLevel);
				this.timeLabel.Text = $"Время выполнения команды: {ip.LastTimeOperation.Seconds} сек. {ip.LastTimeOperation.Milliseconds} мсек.";

				this.loadingLabel.Text = "Завершено";
				pictureBox.Refresh();
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			HistogramForm hf = new HistogramForm();
			ImageProccesing ip = new ImageProccesing((Bitmap)pictureBox.Image);

			hf.BeforeImage = (Bitmap)ip.ConvertToGrayScale().Image.Clone();
			hf.AfterImage = (Bitmap)ip.ConvertToGrayScale().StretchHistogram().Image.Clone();

			hf.ShowDialog();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			MergeImageForm mif = new MergeImageForm();
			mif.ShowDialog();
		}

		#endregion Private Methods

		private void button3_Click(object sender, EventArgs e)
		{
			var fis = new FindIrisForm();
			fis.ShowDialog();
		}
	}
}