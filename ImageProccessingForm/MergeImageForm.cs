﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ImageProccessing;

namespace ImageProccessingForm
{
	public partial class MergeImageForm : Form
	{
		#region Private Fields

		private long maxCountPixel = 0;

		#endregion Private Fields

		#region Public Constructors

		public MergeImageForm()
		{
			InitializeComponent();

			pbSourceImage.Image = new ImageProccesing((Bitmap)pbSourceImage.Image).ConvertToGrayScale().Image;
		}

		#endregion Public Constructors

		#region Private Methods

		private void btnDonorImage_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();

			if (ofd.ShowDialog() == DialogResult.OK)
			{
				pbDonorImage.Image = (Bitmap)Image.FromFile(ofd.FileName);
			}
		}

		private void btnSourceImage_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();

			if (ofd.ShowDialog() == DialogResult.OK)
			{
				pbSourceImage.Image = new ImageProccessing.ImageProccesing((Bitmap)Image.FromFile(ofd.FileName)).ConvertToGrayScale().Image;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pbSourceImage.Image);
			ip.OnProgressChanged += Ip_OnProgressChanged;

			maxCountPixel = pbSourceImage.Image.Width * pbSourceImage.Image.Height;
			progressBar1.Maximum = (int)maxCountPixel;

			var resImage = ip.MergeImages((Bitmap)pbSourceImage.Image, (Bitmap)pbDonorImage.Image);

			pbSourceImage.Image = resImage;
		}

		private void Ip_OnProgressChanged(object sender, ProgressEventArgs e)
		{
			this.progressBar1.Value = (int)e.CurPixProccessing;
		}

		#endregion Private Methods
	}
}