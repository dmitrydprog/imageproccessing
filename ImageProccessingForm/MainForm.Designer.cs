﻿namespace ImageProccessingForm
{
	partial class MainForm
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.title = new System.Windows.Forms.Label();
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.btnOpenFileDialog = new System.Windows.Forms.Button();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.btnNaegative = new System.Windows.Forms.Button();
			this.btnGamma = new System.Windows.Forms.Button();
			this.btnGrayscale = new System.Windows.Forms.Button();
			this.btnBinarizate = new System.Windows.Forms.Button();
			this.btnSmooth = new System.Windows.Forms.Button();
			this.btnLaplas = new System.Windows.Forms.Button();
			this.brnSobel = new System.Windows.Forms.Button();
			this.btnRoberts = new System.Windows.Forms.Button();
			this.btnKirsh = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.timeLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.loadingLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.button3 = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.tableLayoutPanel3.SuspendLayout();
			this.tableLayoutPanel4.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 22);
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(812, 609);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel2.ColumnCount = 1;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.Controls.Add(this.title, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.pictureBox, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 2);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 4);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 3;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(641, 579);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// title
			// 
			this.title.AutoSize = true;
			this.title.Dock = System.Windows.Forms.DockStyle.Fill;
			this.title.Location = new System.Drawing.Point(4, 1);
			this.title.Name = "title";
			this.title.Size = new System.Drawing.Size(633, 13);
			this.title.TabIndex = 0;
			this.title.Text = "Изображение";
			this.title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pictureBox
			// 
			this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox.Image = global::ImageProccessingForm.Properties.Resources._2487756853_2_1;
			this.pictureBox.Location = new System.Drawing.Point(4, 18);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(633, 516);
			this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox.TabIndex = 1;
			this.pictureBox.TabStop = false;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.btnOpenFileDialog, 1, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 541);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 1;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel3.Size = new System.Drawing.Size(633, 34);
			this.tableLayoutPanel3.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(532, 34);
			this.label1.TabIndex = 0;
			this.label1.Text = "Загрузить из файла:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnOpenFileDialog
			// 
			this.btnOpenFileDialog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnOpenFileDialog.Location = new System.Drawing.Point(541, 3);
			this.btnOpenFileDialog.Name = "btnOpenFileDialog";
			this.btnOpenFileDialog.Size = new System.Drawing.Size(89, 28);
			this.btnOpenFileDialog.TabIndex = 1;
			this.btnOpenFileDialog.Text = "...";
			this.btnOpenFileDialog.UseVisualStyleBackColor = true;
			this.btnOpenFileDialog.Click += new System.EventHandler(this.btnOpenFileDialog_Click);
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.ColumnCount = 1;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Controls.Add(this.btnNaegative, 0, 0);
			this.tableLayoutPanel4.Controls.Add(this.btnGamma, 0, 1);
			this.tableLayoutPanel4.Controls.Add(this.btnGrayscale, 0, 2);
			this.tableLayoutPanel4.Controls.Add(this.btnBinarizate, 0, 3);
			this.tableLayoutPanel4.Controls.Add(this.btnSmooth, 0, 4);
			this.tableLayoutPanel4.Controls.Add(this.btnLaplas, 0, 5);
			this.tableLayoutPanel4.Controls.Add(this.brnSobel, 0, 6);
			this.tableLayoutPanel4.Controls.Add(this.btnRoberts, 0, 7);
			this.tableLayoutPanel4.Controls.Add(this.btnKirsh, 0, 8);
			this.tableLayoutPanel4.Controls.Add(this.button1, 0, 9);
			this.tableLayoutPanel4.Controls.Add(this.button2, 0, 10);
			this.tableLayoutPanel4.Controls.Add(this.button3, 0, 11);
			this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel4.Location = new System.Drawing.Point(652, 4);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 12;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(156, 579);
			this.tableLayoutPanel4.TabIndex = 1;
			// 
			// btnNaegative
			// 
			this.btnNaegative.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnNaegative.Location = new System.Drawing.Point(3, 3);
			this.btnNaegative.Name = "btnNaegative";
			this.btnNaegative.Size = new System.Drawing.Size(150, 23);
			this.btnNaegative.TabIndex = 0;
			this.btnNaegative.Text = "Negative";
			this.btnNaegative.UseVisualStyleBackColor = true;
			this.btnNaegative.Click += new System.EventHandler(this.btnNaegative_Click);
			// 
			// btnGamma
			// 
			this.btnGamma.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnGamma.Location = new System.Drawing.Point(3, 32);
			this.btnGamma.Name = "btnGamma";
			this.btnGamma.Size = new System.Drawing.Size(150, 23);
			this.btnGamma.TabIndex = 1;
			this.btnGamma.Text = "Gamma";
			this.btnGamma.UseVisualStyleBackColor = true;
			this.btnGamma.Click += new System.EventHandler(this.btnGamma_Click);
			// 
			// btnGrayscale
			// 
			this.btnGrayscale.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnGrayscale.Location = new System.Drawing.Point(3, 61);
			this.btnGrayscale.Name = "btnGrayscale";
			this.btnGrayscale.Size = new System.Drawing.Size(150, 23);
			this.btnGrayscale.TabIndex = 2;
			this.btnGrayscale.Text = "Convert to grayscale";
			this.btnGrayscale.UseVisualStyleBackColor = true;
			this.btnGrayscale.Click += new System.EventHandler(this.btnGrayscale_Click);
			// 
			// btnBinarizate
			// 
			this.btnBinarizate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnBinarizate.Location = new System.Drawing.Point(3, 90);
			this.btnBinarizate.Name = "btnBinarizate";
			this.btnBinarizate.Size = new System.Drawing.Size(150, 23);
			this.btnBinarizate.TabIndex = 3;
			this.btnBinarizate.Text = "Binarizate";
			this.btnBinarizate.UseVisualStyleBackColor = true;
			this.btnBinarizate.Click += new System.EventHandler(this.btnBinarizate_Click);
			// 
			// btnSmooth
			// 
			this.btnSmooth.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnSmooth.Location = new System.Drawing.Point(3, 119);
			this.btnSmooth.Name = "btnSmooth";
			this.btnSmooth.Size = new System.Drawing.Size(150, 23);
			this.btnSmooth.TabIndex = 4;
			this.btnSmooth.Text = "Smooth";
			this.btnSmooth.UseVisualStyleBackColor = true;
			this.btnSmooth.Click += new System.EventHandler(this.btnSmooth_Click);
			// 
			// btnLaplas
			// 
			this.btnLaplas.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnLaplas.Location = new System.Drawing.Point(3, 148);
			this.btnLaplas.Name = "btnLaplas";
			this.btnLaplas.Size = new System.Drawing.Size(150, 23);
			this.btnLaplas.TabIndex = 5;
			this.btnLaplas.Text = "Laplas filter";
			this.btnLaplas.UseVisualStyleBackColor = true;
			this.btnLaplas.Click += new System.EventHandler(this.btnLaplas_Click);
			// 
			// brnSobel
			// 
			this.brnSobel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.brnSobel.Location = new System.Drawing.Point(3, 177);
			this.brnSobel.Name = "brnSobel";
			this.brnSobel.Size = new System.Drawing.Size(150, 23);
			this.brnSobel.TabIndex = 6;
			this.brnSobel.Text = "Sobel filter";
			this.brnSobel.UseVisualStyleBackColor = true;
			this.brnSobel.Click += new System.EventHandler(this.brnSobel_Click);
			// 
			// btnRoberts
			// 
			this.btnRoberts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnRoberts.Location = new System.Drawing.Point(3, 206);
			this.btnRoberts.Name = "btnRoberts";
			this.btnRoberts.Size = new System.Drawing.Size(150, 23);
			this.btnRoberts.TabIndex = 7;
			this.btnRoberts.Text = "Roberts filter";
			this.btnRoberts.UseVisualStyleBackColor = true;
			this.btnRoberts.Click += new System.EventHandler(this.btnRoberts_Click);
			// 
			// btnKirsh
			// 
			this.btnKirsh.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnKirsh.Location = new System.Drawing.Point(3, 235);
			this.btnKirsh.Name = "btnKirsh";
			this.btnKirsh.Size = new System.Drawing.Size(150, 23);
			this.btnKirsh.TabIndex = 8;
			this.btnKirsh.Text = "Kirsh filter";
			this.btnKirsh.UseVisualStyleBackColor = true;
			this.btnKirsh.Click += new System.EventHandler(this.btnKirsh_Click);
			// 
			// button1
			// 
			this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button1.Location = new System.Drawing.Point(3, 264);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(150, 99);
			this.button1.TabIndex = 9;
			this.button1.Text = "Histogram";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button2.Location = new System.Drawing.Point(3, 369);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(150, 100);
			this.button2.TabIndex = 10;
			this.button2.Text = "MergeImage";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timeLabel,
            this.loadingLabel});
			this.statusStrip1.Location = new System.Drawing.Point(0, 587);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(812, 22);
			this.statusStrip1.TabIndex = 1;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// timeLabel
			// 
			this.timeLabel.Name = "timeLabel";
			this.timeLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// loadingLabel
			// 
			this.loadingLabel.Name = "loadingLabel";
			this.loadingLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// button3
			// 
			this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button3.Location = new System.Drawing.Point(3, 475);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(150, 101);
			this.button3.TabIndex = 11;
			this.button3.Text = "Find iris";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(812, 609);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "MainForm";
			this.Text = "ImageProccessing";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			this.tableLayoutPanel4.ResumeLayout(false);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Label title;
		private System.Windows.Forms.PictureBox pictureBox;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnOpenFileDialog;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.Button btnNaegative;
		private System.Windows.Forms.Button btnGamma;
		private System.Windows.Forms.Button btnGrayscale;
		private System.Windows.Forms.Button btnBinarizate;
		private System.Windows.Forms.Button btnSmooth;
		private System.Windows.Forms.Button btnLaplas;
		private System.Windows.Forms.Button brnSobel;
		private System.Windows.Forms.Button btnRoberts;
		private System.Windows.Forms.Button btnKirsh;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel timeLabel;
		private System.Windows.Forms.ToolStripStatusLabel loadingLabel;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
	}
}

