﻿using System;
using System.Windows.Forms;

namespace ImageProccessingForm
{
	internal static class Program
	{
		#region Private Methods

		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}

		#endregion Private Methods
	}
}