﻿namespace ImageProccessingForm
{
	partial class MergeImageForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MergeImageForm));
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.btnSourceImage = new System.Windows.Forms.Button();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.btnDonorImage = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.pbSourceImage = new System.Windows.Forms.PictureBox();
			this.pbDonorImage = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pbSourceImage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbDonorImage)).BeginInit();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.button1, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(906, 635);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 1;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.pbSourceImage, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.btnSourceImage, 0, 1);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(447, 565);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// btnSourceImage
			// 
			this.btnSourceImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnSourceImage.Location = new System.Drawing.Point(3, 511);
			this.btnSourceImage.Name = "btnSourceImage";
			this.btnSourceImage.Size = new System.Drawing.Size(441, 51);
			this.btnSourceImage.TabIndex = 1;
			this.btnSourceImage.Text = "Загрузить исходное изображение";
			this.btnSourceImage.UseVisualStyleBackColor = true;
			this.btnSourceImage.Click += new System.EventHandler(this.btnSourceImage_Click);
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 1;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Controls.Add(this.pbDonorImage, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.btnDonorImage, 0, 1);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(456, 3);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(447, 565);
			this.tableLayoutPanel3.TabIndex = 1;
			// 
			// btnDonorImage
			// 
			this.btnDonorImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnDonorImage.Location = new System.Drawing.Point(3, 511);
			this.btnDonorImage.Name = "btnDonorImage";
			this.btnDonorImage.Size = new System.Drawing.Size(441, 51);
			this.btnDonorImage.TabIndex = 1;
			this.btnDonorImage.Text = "Загрузить изображение донор (цветное)";
			this.btnDonorImage.UseVisualStyleBackColor = true;
			this.btnDonorImage.Click += new System.EventHandler(this.btnDonorImage_Click);
			// 
			// button1
			// 
			this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button1.Location = new System.Drawing.Point(3, 574);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(447, 58);
			this.button1.TabIndex = 2;
			this.button1.Text = "Перенос цвета";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Controls.Add(this.label1);
			this.flowLayoutPanel1.Controls.Add(this.progressBar1);
			this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(456, 574);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(444, 58);
			this.flowLayoutPanel1.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(59, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Прогресс:";
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(3, 16);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(438, 33);
			this.progressBar1.TabIndex = 1;
			// 
			// pbSourceImage
			// 
			this.pbSourceImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbSourceImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbSourceImage.Image = ((System.Drawing.Image)(resources.GetObject("pbSourceImage.Image")));
			this.pbSourceImage.Location = new System.Drawing.Point(3, 3);
			this.pbSourceImage.Name = "pbSourceImage";
			this.pbSourceImage.Size = new System.Drawing.Size(441, 502);
			this.pbSourceImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbSourceImage.TabIndex = 0;
			this.pbSourceImage.TabStop = false;
			// 
			// pbDonorImage
			// 
			this.pbDonorImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbDonorImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbDonorImage.Image = global::ImageProccessingForm.Properties.Resources.pole_leto_trava_gory_krasivo_84743_300x300;
			this.pbDonorImage.Location = new System.Drawing.Point(3, 3);
			this.pbDonorImage.Name = "pbDonorImage";
			this.pbDonorImage.Size = new System.Drawing.Size(441, 502);
			this.pbDonorImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbDonorImage.TabIndex = 0;
			this.pbDonorImage.TabStop = false;
			// 
			// MergeImageForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(906, 635);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "MergeImageForm";
			this.Text = "MergeImageForm";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pbSourceImage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbDonorImage)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.PictureBox pbSourceImage;
		private System.Windows.Forms.Button btnSourceImage;
		private System.Windows.Forms.PictureBox pbDonorImage;
		private System.Windows.Forms.Button btnDonorImage;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ProgressBar progressBar1;
	}
}