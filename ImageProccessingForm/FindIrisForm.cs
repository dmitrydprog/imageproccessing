﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageProccessing;
using ImageProccessing.FastImage;
using System.Numerics;
using System.Diagnostics;

namespace ImageProccessingForm
{
	public partial class FindIrisForm : Form
	{
		int[] histArray;

		public FindIrisForm()
		{
			InitializeComponent();
			GetHisto();
		}

		private void GetHisto()
		{
			chart1.Series[0].Points.Clear();

			var ip = new ImageProccesing((Bitmap)pictureBox1.Image);
			pictureBox1.Image = ip.ConvertToGrayScale().Image;

			histArray = new int[256];
			ip.GetHistogram(out histArray);

			for (int i = 0; i < histArray.Length; i++)
			{
				chart1.Series[0].Points.AddXY(i, histArray[i]);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			var ofd = new OpenFileDialog();

			if (ofd.ShowDialog() == DialogResult.OK)
			{
				pictureBox1.Image = new ImageProccesing((Bitmap)Bitmap.FromFile(ofd.FileName)).ConvertToGrayScale().Image;
			}

			GetHisto();
        }

		private void button2_Click(object sender, EventArgs e)
		{
			BinarizateFormSetting bfs = new BinarizateFormSetting();

			if (bfs.ShowDialog() == DialogResult.OK)
			{
				var ip = new ImageProccesing((Bitmap)pictureBox1.Image.Clone()).Binarizate(bfs.LimValue);
				pictureBox2.Image = ip.Image;

				find(bfs.LimValue);
			}
		}

		private void button1_Click_1(object sender, EventArgs e)
		{
			if (histArray == null)
				return;

			float min = 100;
			int minIndex = 0;
			for (int i = 0; i < 50; i++)
			{
				if (histArray[i + 1] - histArray[i] < min)
				{
					min = histArray[i + 1] - histArray[i];
					minIndex = i;
				}
			}

			for (int i = minIndex; i < 254; i++)
			{
				if (histArray[i] - histArray[i + 1] > 0)
				{
					minIndex = i;
					break;
				}
			}

			find(minIndex);
		}

		private void find(int minIndex)
		{
			var ip = new ImageProccesing((Bitmap)pictureBox1.Image.Clone()).Binarizate((byte)(minIndex + 8));
			pictureBox2.Image = ip.Image;

			int lineX = 0,
				lineY = 0,
				tmp = 0,
				tmp2 = 0,
				rAote = 0,
				rIris = 0;

			using (var fimage = new FImage((Bitmap)pictureBox2.Image))
			using (var fimages = new FImage((Bitmap)pictureBox1.Image.Clone()))
			{
				for (int y = 0; y < fimage.Bitmap.Height; y++)
				{
					for (int x = 0; x < fimage.Bitmap.Width; x++)
					{
						if (fimage[x, y].R == 0) tmp++;
					}

					if (tmp > tmp2)
					{
						tmp2 = tmp;
						lineY = y;
					}

					tmp = 0;
				}

				tmp = tmp2 = 0;

				for (int x = 0; x < fimage.Bitmap.Width; x++)
				{
					for (int y = 0; y < fimage.Bitmap.Height; y++)
					{
						if (fimage[x, y].R == 0) tmp++;
					}

					if (tmp > tmp2)
					{
						tmp2 = tmp;
						lineX = x;
					}

					tmp = 0;
				}

				for (int x = lineX; x < fimage.Bitmap.Width; x++)
				{
					if (fimage[x, lineY].R == 0) continue;

					rAote = x - lineX;
					break;
				}

				rIris = 0;

				for (int x = lineX - rAote - 1; x > 5; x--)
				{
					if (Math.Abs(fimages[x, lineY].R - fimages[x - 5, lineY].R) >= 80)
					{
						rIris = x - lineX - rAote - 1;
						break;
					}
				}

				if (rIris == 0)
					rIris = (int)(rAote * 3.5);

				rIris = Math.Abs(rIris);

				for (int x = 0; x < fimage.Bitmap.Width; x++)
				{
					fimages[x, lineY] = Color.Red;
				}

				for (int y = 0; y < fimage.Bitmap.Height; y++)
				{
					fimages[lineX, y] = Color.Red;
				}

				try
				{
					for (double ang = 0; ang < 2 * Math.PI; ang += 0.001)
					{
						fimages[
							(int)(Math.Cos(ang) * rIris) + lineX,
							(int)(Math.Sin(ang) * rIris) + lineY]
							= Color.Red;
					}

					for (double ang = 0; ang < 2 * Math.PI; ang += 0.001)
					{
						fimages[
							(int)(Math.Cos(ang) * rAote) + lineX,
							(int)(Math.Sin(ang) * rAote) + lineY]
							= Color.Red;
					}
				}
				catch
				{

				}

				pictureBox1.Image = (Image)fimages.Bitmap.Clone();
			}
		}
	}
}
