﻿using System;
using System.Windows.Forms;

namespace ImageProccessingForm
{
	public partial class BinarizateFormSetting : Form
	{
		#region Public Constructors

		public BinarizateFormSetting()
		{
			InitializeComponent();
		}

		#endregion Public Constructors

		#region Public Properties

		public byte LimValue { get; private set; }

		#endregion Public Properties

		#region Private Methods

		private void button1_Click(object sender, EventArgs e)
		{
			this.LimValue = (byte)this.numericUpDown1.Value;
		}

		#endregion Private Methods
	}
}