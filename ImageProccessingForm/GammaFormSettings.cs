﻿using System;
using System.Windows.Forms;

namespace ImageProccessingForm
{
	public partial class GammaFormSettings : Form
	{
		#region Public Constructors

		public GammaFormSettings()
		{
			InitializeComponent();
		}

		#endregion Public Constructors

		#region Public Properties

		public float Gamma { get; private set; }

		#endregion Public Properties

		#region Private Methods

		private void button1_Click(object sender, EventArgs e)
		{
			this.Gamma = (float)numericUpDown1.Value;
		}

		#endregion Private Methods
	}
}