﻿using System.Drawing;
using System.Windows.Forms;
using ImageProccessing.FastImage;
using System.Linq;
using ImageProccessing;
using System.Threading.Tasks;
using System;
using System.Threading.Tasks;

namespace ImageProccessingForm
{
	public partial class HistogramForm : Form
	{
		#region Public Constructors

		public HistogramForm()
		{
			InitializeComponent();
		}

		#endregion Public Constructors

		#region Public Properties

		public Bitmap AfterImage
		{
			set
			{
				pbAfter.Image = value;
				chartAfter.Series[0].Points.Clear();

				using (var fimage = new FImage(value))
				{
					int[] arr = new int[256];

					for (int x = 0; x < fimage.Bitmap.Width; x++)
					{
						for (int y = 0; y < fimage.Bitmap.Height; y++)
						{
							arr[fimage[x, y].R]++;
						}
					}

					for (int i = 0; i < 256; i++)
					{
						chartAfter.Series[0].Points.AddXY(i, arr[i]);
					}
				}
			}
		}

		public Bitmap BeforeImage
		{
			set
			{
				pbBefore.Image = value;
				chartBefore.Series[0].Points.Clear();

				using (var fimage = new FImage(value))
				{
					int[] arr = new int[256];

					for (int x = 0; x < fimage.Bitmap.Width; x++)
					{
						for (int y = 0; y < fimage.Bitmap.Height; y++)
						{
							arr[fimage[x, y].R]++;
						}
					}

					for (int i = 0; i < 256; i++)
					{
						chartBefore.Series[0].Points.AddXY(i, arr[i]);
					}
				}
			}
		}

		#endregion Public Properties

		private void trackBar1_Scroll(object sender, System.EventArgs e)
		{
		}

		private void trackBar1_MouseUp(object sender, MouseEventArgs e)
		{
			ImageProccesing ip = new ImageProccesing((Bitmap)pbBefore.Image.Clone());
			ip.StretchHistogram(trackBar1.Value);

			pbAfter.Image = ip.Image;
			this.AfterImage = ip.Image;
		}
	}
}