<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ColorMine.ColorSpaces;
using ImageProccessing.FastImage;

namespace ImageProccessing
{
	/// <summary>
	/// Класс отвечающий за обработку изображений.
	/// </summary>
	public class ImageProccesing
	{
		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр класса <see cref="ImageProccessing.ImageProccesing"/>.
		/// </summary>
		/// <param name="image">Изображение для обработки.</param>
		public ImageProccesing(Bitmap image)
		{
			this.Image = image;
		}

		#endregion Public Constructors

		#region Public Events

		public event EventHandler<ProgressEventArgs> OnProgressChanged;

		#endregion Public Events

		#region Public Properties

		/// <summary>
		/// Изображение доступное только для чтения.
		/// </summary>
		/// <value>Изображение.</value>
		public Bitmap Image { get; private set; }

		/// <summary>
		/// Время последней операции доступное только для чтения.
		/// </summary>
		/// <value>Время последний операции.</value>
		public TimeSpan LastTimeOperation { get; private set; }

		#endregion Public Properties

		#region Public Methods

		/// <summary>
		/// Бинаризация изображения методом пороговой бинаризации.
		/// </summary>
		/// <param name="limValue">Порог бинаризации.</param>
		public ImageProccesing Binarizate(byte limValue = 127)
		{
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						var midValue = (fimage[i, j].R + fimage[i, j].G + fimage[i, j].B) / 3;

						if (midValue < limValue)
						{
							fimage[i, j] = Color.FromArgb(0, 0, 0);
						}
						else
						{
							fimage[i, j] = Color.FromArgb(255, 255, 255);
						}
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Преобразовывает изображение в черно белое.
		/// </summary>
		/// <returns>Черно белое изображение.</returns>
		public ImageProccesing ConvertToGrayScale()
		{
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						var midValue = (fimage[i, j].R + fimage[i, j].G + fimage[i, j].B) / 3;
						fimage[i, j] = Color.FromArgb(
							red: midValue,
							green: midValue,
							blue: midValue
						);
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Получает гистрограмму на основне текущего изображение.
		/// </summary>
		/// <param name="histoArray">Гистрограмма изображения.</param>
		/// <returns>Текущее изображение.</returns>
		[Obsolete("Useless method")]
		public ImageProccesing GetHistogram(out int[] histoArray)
		{
			histoArray = new int[256];

			this.ConvertToGrayScale();

			using (var fimage = new FImage(this.Image))
				for (int x = 0; x < this.Image.Width; x++)
					for (int y = 0; y < this.Image.Height; y++)
						histoArray[fimage[x, y].R]++;

			return this;
		}

		/// <summary>
		/// Фильтр Кирша.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Kirsh()
		{
			double[,] maskX = new double[3, 3]
			{
				{ -3, -3, -3 },
				{ -3, 0, -3 },
				{ 5, 5, 5 }
			};

			double[,] maskY = new double[3, 3]
			{
				{ 5, -3, -3 },
				{ 5, 0, -3 },
				{ 5, -3, -3 }
			};

			return _filtering2(maskX, maskY);
		}

		/// <summary>
		/// Фильрт Лапласа.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Laplas()
		{
			double[,] filter = new double[3, 3]
			{
				{ -1, -1, -1 },
				{ -1,  8, -1 },
				{ -1, -1, -1 }
			};

			return _filtering2(filter, filter);
		}

		public Bitmap MergeImages(Bitmap sourceImage, Bitmap donorImage)
		{
			GC.AddMemoryPressure(1024 * 50);
			GC.CancelFullGCNotification();

			sourceImage = (Bitmap)sourceImage.Clone();
			donorImage = (Bitmap)donorImage.Clone();

			var infoGridDonor = this.getInfoGrid(donorImage);

			long pixelsProccesing = 0;

			double[] getRect = null;
			InfoGrid bestInfo = null;

			using (var fimages = new FImage((Bitmap)sourceImage.Clone()))
			using (var fimaged = new FImage((Bitmap)donorImage.Clone()))
			{
				for (int x = 0; x < fimages.Bitmap.Width; x++)
				{
					for (int y = 0; y < fimages.Bitmap.Height; y++)
					{
						getRect = this.getRectVal(x, y);
						var exp = this.getExpectedValue(getRect);
						var disp = this.getDispersion(getRect, exp);

						var minValue = double.MaxValue;
						bestInfo = new InfoGrid();
						foreach (var info in infoGridDonor)
						{
							var curMinValue = Math.Abs(info.Dispersion - disp) + Math.Abs(info.ExpectedValue - exp);

							if (curMinValue < minValue)
							{
								minValue = curMinValue;
								bestInfo = info;
							}
						}

						var donorColorRGB = fimaged[bestInfo.X, bestInfo.Y];
						var sourceColorRGB = fimages[x, y];

						var donorColorLAB = new Rgb()
						{
							R = donorColorRGB.R,
							G = donorColorRGB.G,
							B = donorColorRGB.B
						}.To<Lab>();

						var sourceColorLAB = new Rgb()
						{
							R = sourceColorRGB.R,
							G = sourceColorRGB.G,
							B = sourceColorRGB.B
						}.To<Lab>();

						sourceColorLAB.A = donorColorLAB.A;
						sourceColorLAB.B = donorColorLAB.B;

						var newColor = sourceColorLAB.ToRgb();

						fimages[x, y] = Color.FromArgb(
							red: (int)newColor.R,
							green: (int)newColor.G,
							blue: (int)newColor.B
							);

						if (this.OnProgressChanged != null) this.OnProgressChanged(this, new ProgressEventArgs(pixelsProccesing++));
					}

					Application.DoEvents();
				}

				return fimages.Bitmap;
			}
		}

		/// <summary>
		/// Инвертирует цвета изображения.
		/// </summary>
		public ImageProccesing Negative()
		{
			Color color;
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						color = fimage[i, j];
						color = Color.FromArgb(255 - color.R, 255 - color.G, 255 - color.B);
						fimage[i, j] = color;
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Фильтр Робертса.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Roberts()
		{
			double[,] filterX = new double[2, 2]
			{
				{ 1, 0 },
				{ 0, -1 }
			};

			double[,] filterY = new double[2, 2]
			{
				{ 0, 1 },
				{ -1, 0 }
			};

			return _filtering2(filterX, filterY);
		}

		/// <summary>
		/// Устанавливает гамму для изображения. Задается дельта гаммы, то есть значения 0.5
		/// увеличивает гамму от текущей на 50%, а значение -0.8 затемняет изображения на 80% от текущего.
		/// </summary>
		/// <param name="gamma">Гамма может быть числом на промежутке от [-1; 1].</param>
		public ImageProccesing SetGamma(float gamma)
		{
			if (gamma < -1f || gamma > 1f)
			{
				throw new ArgumentException("Неверные значения аргумента, гамма должны быть в пределах от -1 до 1");
			}

			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						if (gamma < 0)
						{
							fimage[i, j] = Color.FromArgb(
								red: (byte)(fimage[i, j].R + fimage[i, j].R * gamma),
								green: (byte)(fimage[i, j].G + fimage[i, j].G * gamma),
								blue: (byte)(fimage[i, j].B + fimage[i, j].B * gamma)
							);
						}
						else
						{
							fimage[i, j] = Color.FromArgb(
								red: (byte)(fimage[i, j].R + (255 - fimage[i, j].R) * gamma),
								green: (byte)(fimage[i, j].G + (255 - fimage[i, j].G) * gamma),
								blue: (byte)(fimage[i, j].B + (255 - fimage[i, j].B) * gamma)
							);
						}
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Сглаживание изображения.
		/// </summary>
		/// <param name="strength">Сила сглаживания.</param>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Smooth(int strength = 7)
		{
			var rand = new Random();
			var filter = new double[strength, strength];

			for (int i = 0; i < strength; i++)
			{
				for (int j = 0; j < strength; j++)
				{
					filter[i, j] = rand.NextDouble();
				}
			}

			return this._filtering(filter);
		}

		/// <summary>
		/// Фильтр Собеля.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Sobel()
		{
			double[,] filterX = new double[3, 3]
			{
				{ -1, 0, 1 },
				{ -2, 0, 2 },
				{ -1, 0, 1 }
			};

			double[,] filterY = new double[3, 3]
			{
				{ -1, -2, -1 },
				{ 0, 0, 0 },
				{ 1, 2, 1 }
			};

			return _filtering2(filterX, filterY);
		}

		/// <summary>
		/// Растяжение гистограммы.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing StretchHistogram(float right = 255f)
		{
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				long[] GrHst = new long[256]; long HstValue = 0;
				long[] GrSum = new long[256]; long SumValue = 0;

				for (int row = 0; row < fimage.Bitmap.Height; row++)
					for (int col = 0; col < fimage.Bitmap.Width; col++)
					{
						HstValue = (long)(255 * fimage[row, col].GetBrightness());
						GrHst[HstValue]++;
					}

				for (int level = 0; level < 256; level++)
				{
					SumValue += GrHst[level];
					GrSum[level] = SumValue;
				}

				for (int row = 0; row < fimage.Bitmap.Height; row++)
					for (int col = 0; col < fimage.Bitmap.Width; col++)
					{
						Color clr = fimage[row, col];
						HstValue = (long)(255f * clr.GetBrightness());
						HstValue = (long)(right / (fimage.Bitmap.Width * fimage.Bitmap.Height) * GrSum[HstValue] 
							- HstValue + Math.Abs(255f - right)/2f);
						int R = (int)Math.Max(0, Math.Min(255, clr.R + HstValue));

						fimage[row, col] = Color.FromArgb(R, R, R);
					}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		#endregion Public Methods

		#region Private Methods

		/// <summary>
		/// Фильтрует изображение используя фильтр.
		/// </summary>
		/// <param name="kernel">Фильтр.</param>
		/// <returns>Изображение после применение эффекта.</returns>
		private ImageProccesing _filtering(double[,] kernel)
		{
			var width = this.Image.Width;
			var height = this.Image.Height;
			var kernelWidth = kernel.GetLength(0);
			var kernelHeight = kernel.GetLength(1);

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						double rSum = 0;
						double gSum = 0;
						double bSum = 0;
						double kSum = 0;

						for (int iK = 0; iK < kernelWidth; iK++)
						{
							for (int jK = 0; jK < kernelHeight; jK++)
							{
								int pixelPosX = i + (iK - (kernelWidth / 2));
								int pixelPosY = j + (jK - (kernelWidth / 2));

								if ((pixelPosX < 0) || (pixelPosX >= width)
									|| (pixelPosY < 0) || (pixelPosY >= height))
									continue;

								double kernelVal = kernel[iK, jK];

								rSum += fimage[pixelPosX, pixelPosY].R * kernelVal;
								gSum += fimage[pixelPosX, pixelPosY].G * kernelVal;
								bSum += fimage[pixelPosX, pixelPosY].B * kernelVal;

								kSum += kernelVal;
							}
						}

						kSum = kSum <= 0 ? 1 : kSum;

						rSum /= kSum;
						if (rSum < 0) rSum = 0;
						if (rSum > 255) rSum = 255;

						gSum /= kSum;
						if (gSum < 0) gSum = 0;
						if (gSum > 255) gSum = 255;

						bSum /= kSum;
						if (bSum < 0) bSum = 0;
						if (bSum > 255) bSum = 255;

						fimage[i, j] = Color.FromArgb(
							red: (byte)rSum,
							green: (byte)gSum,
							blue: (byte)bSum
						);
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Фильтрует изображения используя два фильтра.
		/// </summary>
		/// <param name="kernel1">Горизонтальный фильтр.</param>
		/// <param name="kernel2">Вертикальный фильтр.</param>
		/// <returns>Изображение после применение эффекта.</returns>
		private ImageProccesing _filtering2(double[,] kernel1, double[,] kernel2)
		{
			var width = this.Image.Width;
			var height = this.Image.Height;

			var newBitmap = new Bitmap(this.Image.Width, this.Image.Height, this.Image.PixelFormat);

			using (var fimage = new FImage(this.Image))
			using (var fimageNew = new FImage(newBitmap))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int x = 1; x < fimage.Bitmap.Width - 1; x++)
				{
					for (int y = 1; y < fimage.Bitmap.Height - 1; y++)
					{
						double sumX = 0, sumY = 0, sum = 0;

						for (int i = -1; i < kernel1.GetLength(0) - 1; i++)
						{
							for (int j = -1; j < kernel1.GetLength(1) - 1; j++)
							{
								sumX += fimage[x + i, y + j].R * kernel1[i + 1, j + 1];
								sumY += fimage[x + i, y + j].R * kernel2[i + 1, j + 1];
							}
						}

						sum = Math.Sqrt(sumX * sumX + sumY * sumY);
						sum = sum > 255 ? 255 : sum < 0 ? 0 : sum;

						fimageNew[x, y] = Color.FromArgb((byte)sum, (byte)sum, (byte)sum);
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;

				this.Image = (Bitmap)fimageNew.Bitmap.Clone();
			}

			return this;
		}

		/// <summary>
		/// Вычисление дисперсиии заданной велечины.
		/// </summary>
		/// <param name="x">Значения величины.</param>
		/// <param name="p">Вероятности.</param>
		/// <returns>Дисперсия.</returns>
		private double getDispersion(double[] x, double exp)
		{
			var expectedValue = this.getExpectedValue(x.Select(e => e * e).ToArray());

			return expectedValue - exp * exp;
		}

		/// <summary>
		/// Вычисление математического ожидания заданной величины.
		/// </summary>
		/// <param name="x">Значение величины.</param>
		/// <param name="p">Вероятности.</param>
		/// <returns>Математическое ожидание.</returns>
		private double getExpectedValue(double[] x)
		{
			var dict = new Dictionary<double, int>();
			foreach (var val in x)
				if (dict.ContainsKey(val)) dict[val]++;
				else dict.Add(val, 1);

			return x.Select(e => e * (dict[e] / (double)x.Length)).Sum();
		}

		/// <summary>
		/// Получение черной-белой сигнатуры изображения.
		/// </summary>
		/// <returns>Сигнатура изображения.</returns>
		[Obsolete()]
		private double[] getGrayScaleSignature()
		{
			this.ConvertToGrayScale();

			double[] histoGramImage = new double[256];
			double[] signatureImage = new double[128];

			using (var fimage = new FImage(this.Image))
			{
				for (int x = 0; x < fimage.Bitmap.Width; x++)
					for (int y = 0; y < fimage.Bitmap.Height; y++)
						histoGramImage[fimage[x, y].R]++;

				for (int i = 0; i < 256; i += 2)
					histoGramImage[i] += histoGramImage[i + 1];

				for (int i = 0; i < 128; i++)
					signatureImage[i] = histoGramImage[i * 2];

				signatureImage = signatureImage.Select(x => x * (1 / signatureImage.Sum())).ToArray();
			}

			double test = signatureImage.Sum();
			return signatureImage;
		}

		private InfoGrid[] getInfoGrid(Bitmap image)
		{
			var rand = new Random(DateTime.Now.Millisecond);

			List<InfoGrid> listInfoGrid = new List<InfoGrid>();

			int randX, randY;
			for (int x = 0; x < image.Width; x += image.Width / 15 - 1)
			{
				for (int y = 0; y < image.Height; y += image.Height / 15 - 1)
				{
					randX = rand.Next(x, x + image.Width / 15);
					randY = rand.Next(y, y + image.Height / 15);

					if (randX > image.Width - 1 || randY > image.Height - 1 ||
						 randX < 0 || randY < 0)
					{
						x -= x == 0 ? 0 : 1;
						y -= y == 0 ? 0 : 1;

						continue;
					}

					var rectVal = this.getRectVal(randX, randY);
					var expect = this.getExpectedValue(rectVal);
					var disp = this.getDispersion(rectVal, expect);

					listInfoGrid.Add(new InfoGrid(randX, randY, expect, disp));
				}
			}

			return listInfoGrid.ToArray();
		}

		private double[] getRectVal(int centerX, int centerY)
		{
			List<double> pixelVal = new List<double>();

			using (var fimage = new FImage(this.Image))
				for (int i = centerX - 12; i < centerX + 12; i++)
					for (int j = centerY - 12; j < centerY + 12; j++)
					{
						var ii = i - (centerX - 12);
						var jj = j - (centerY - 12);

						if (i < 0 || j < 0 ||
							ii < 0 || jj < 0 ||
							i > this.Image.Width - 1 || j > this.Image.Height - 1 ||
							ii > this.Image.Width - 1 || jj > this.Image.Height - 1)
							continue;

						pixelVal.Add(new Rgb() { R = fimage[i, j].R, G = fimage[i, j].G, B = fimage[i, j].B }.To<Lab>().L);
					}

			return pixelVal.ToArray();
		}

		#endregion Private Methods
	}

	public class ProgressEventArgs
	{
		#region Public Constructors

		public ProgressEventArgs(long curPixProccessing)
		{
			this.CurPixProccessing = curPixProccessing;
		}

		#endregion Public Constructors

		#region Public Properties

		public long CurPixProccessing { get; private set; }

		#endregion Public Properties
	}

	/// <summary>
	/// Структура необходимая для прохода изображения сеткой.
	/// </summary>
	internal class InfoGrid
	{
		#region Public Constructors

		public InfoGrid(int x, int y, double expectedValue, double dispersion)
		{
			this.X = x;
			this.Y = y;
			this.ExpectedValue = expectedValue;
			this.Dispersion = dispersion;
		}

		public InfoGrid()
		{
			this.X = 0;
			this.Y = 0;
			this.ExpectedValue = 0;
			this.Dispersion = 0;
		}

		#endregion Public Constructors

		#region Public Properties

		/// <summary>
		/// Дисперсия.
		/// </summary>
		public double Dispersion { get; set; }

		/// <summary>
		/// Математическое ожидание.
		/// </summary>
		public double ExpectedValue { get; set; }

		/// <summary>
		/// Координата X.
		/// </summary>
		public int X { get; set; }

		/// <summary>
		/// Координата Y.
		/// </summary>
		public int Y { get; set; }

		#endregion Public Properties
	}
=======
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ColorMine.ColorSpaces;
using ImageProccessing.FastImage;

namespace ImageProccessing
{
	/// <summary>
	/// Класс отвечающий за обработку изображений.
	/// </summary>
	public class ImageProccesing
	{
		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр класса <see cref="ImageProccessing.ImageProccesing"/>.
		/// </summary>
		/// <param name="image">Изображение для обработки.</param>
		public ImageProccesing(Bitmap image)
		{
			this.Image = image;
		}

		#endregion Public Constructors

		#region Public Events

		/// <summary>
		/// Отображение прогресса выполнения обработки изображения.
		/// </summary>
		public event EventHandler<ProgressEventArgs> OnProgressChanged;

		#endregion Public Events

		#region Public Properties

		/// <summary>
		/// Изображение доступное только для чтения.
		/// </summary>
		/// <value>Изображение.</value>
		public Bitmap Image { get; private set; }

		/// <summary>
		/// Время последней операции доступное только для чтения.
		/// </summary>
		/// <value>Время последний операции.</value>
		public TimeSpan LastTimeOperation { get; private set; }

		#endregion Public Properties

		#region Public Methods

		/// <summary>
		/// Бинаризация изображения методом пороговой бинаризации.
		/// </summary>
		/// <param name="limValue">Порог бинаризации.</param>
		public ImageProccesing Binarizate(byte limValue = 127)
		{
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						var midValue = (fimage[i, j].R + fimage[i, j].G + fimage[i, j].B) / 3;

						if (midValue < limValue)
						{
							fimage[i, j] = Color.FromArgb(0, 0, 0);
						}
						else
						{
							fimage[i, j] = Color.FromArgb(255, 255, 255);
						}
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Преобразовывает изображение в черно белое.
		/// </summary>
		/// <returns>Черно белое изображение.</returns>
		public ImageProccesing ConvertToGrayScale()
		{
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						var midValue = (fimage[i, j].R + fimage[i, j].G + fimage[i, j].B) / 3;
						fimage[i, j] = Color.FromArgb(
							red: midValue,
							green: midValue,
							blue: midValue
						);
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Получает гистрограмму на основне текущего изображение.
		/// </summary>
		/// <param name="histoArray">Гистрограмма изображения.</param>
		/// <returns>Текущее изображение.</returns>
		[Obsolete("Useless method")]
		public ImageProccesing GetHistogram(out int[] histoArray)
		{
			histoArray = new int[256];

			this.ConvertToGrayScale();

			using (var fimage = new FImage(this.Image))
				for (int x = 0; x < this.Image.Width; x++)
					for (int y = 0; y < this.Image.Height; y++)
						histoArray[fimage[x, y].R]++;

			return this;
		}

		/// <summary>
		/// Фильтр Кирша.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Kirsh()
		{
			double[,] maskX = new double[3, 3]
			{
				{ -3, -3, -3 },
				{ -3, 0, -3 },
				{ 5, 5, 5 }
			};

			double[,] maskY = new double[3, 3]
			{
				{ 5, -3, -3 },
				{ 5, 0, -3 },
				{ 5, -3, -3 }
			};

			return _filtering2(maskX, maskY);
		}

		/// <summary>
		/// Фильрт Лапласа.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Laplas()
		{
			double[,] filter = new double[3, 3]
			{
				{ -1, -1, -1 },
				{ -1,  8, -1 },
				{ -1, -1, -1 }
			};

			return _filtering2(filter, filter);
		}

		public Bitmap MergeImages(Bitmap sourceImage, Bitmap donorImage)
		{
			GC.AddMemoryPressure(1024 * 50);
			GC.CancelFullGCNotification();

			sourceImage = (Bitmap)sourceImage.Clone();
			donorImage = (Bitmap)donorImage.Clone();

			var infoGridDonor = this.getInfoGrid(donorImage);

			long pixelsProccesing = 0;

			double[] getRect = null;
			InfoGrid bestInfo = null;

			using (var fimages = new FImage((Bitmap)sourceImage.Clone()))
			using (var fimaged = new FImage((Bitmap)donorImage.Clone()))
			{
				for (int x = 0; x < fimages.Bitmap.Width; x++)
				{
					for (int y = 0; y < fimages.Bitmap.Height; y++)
					{
						getRect = this.getRectVal(x, y);
						var exp = this.getExpectedValue(getRect);
						var disp = this.getDispersion(getRect, exp);

						var minValue = double.MaxValue;
						bestInfo = new InfoGrid();
						foreach (var info in infoGridDonor)
						{
							var curMinValue = Math.Abs(info.Dispersion - disp) + Math.Abs(info.ExpectedValue - exp);

							if (curMinValue < minValue)
							{
								minValue = curMinValue;
								bestInfo = info;
							}
						}

						var donorColorRGB = fimaged[bestInfo.X, bestInfo.Y];
						var sourceColorRGB = fimages[x, y];

						var donorColorLAB = new Rgb()
						{
							R = donorColorRGB.R,
							G = donorColorRGB.G,
							B = donorColorRGB.B
						}.To<Lab>();

						var sourceColorLAB = new Rgb()
						{
							R = sourceColorRGB.R,
							G = sourceColorRGB.G,
							B = sourceColorRGB.B
						}.To<Lab>();

						sourceColorLAB.A = donorColorLAB.A;
						sourceColorLAB.B = donorColorLAB.B;

						var newColor = sourceColorLAB.ToRgb();

						fimages[x, y] = Color.FromArgb(
							red: (int)newColor.R,
							green: (int)newColor.G,
							blue: (int)newColor.B
							);

						if (this.OnProgressChanged != null) this.OnProgressChanged(this, new ProgressEventArgs(pixelsProccesing++));
					}

					Application.DoEvents();
				}

				return fimages.Bitmap;
			}
		}

		/// <summary>
		/// Инвертирует цвета изображения.
		/// </summary>
		public ImageProccesing Negative()
		{
			Color color;
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						color = fimage[i, j];
						color = Color.FromArgb(255 - color.R, 255 - color.G, 255 - color.B);
						fimage[i, j] = color;
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Фильтр Робертса.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Roberts()
		{
			double[,] filterX = new double[2, 2]
			{
				{ 1, 0 },
				{ 0, -1 }
			};

			double[,] filterY = new double[2, 2]
			{
				{ 0, 1 },
				{ -1, 0 }
			};

			return _filtering2(filterX, filterY);
		}

		/// <summary>
		/// Устанавливает гамму для изображения. Задается дельта гаммы, то есть значения 0.5
		/// увеличивает гамму от текущей на 50%, а значение -0.8 затемняет изображения на 80% от текущего.
		/// </summary>
		/// <param name="gamma">Гамма может быть числом на промежутке от [-1; 1].</param>
		public ImageProccesing SetGamma(float gamma)
		{
			if (gamma < -1f || gamma > 1f)
			{
				throw new ArgumentException("Неверные значения аргумента, гамма должны быть в пределах от -1 до 1");
			}

			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						if (gamma < 0)
						{
							fimage[i, j] = Color.FromArgb(
								red: (byte)(fimage[i, j].R + fimage[i, j].R * gamma),
								green: (byte)(fimage[i, j].G + fimage[i, j].G * gamma),
								blue: (byte)(fimage[i, j].B + fimage[i, j].B * gamma)
							);
						}
						else
						{
							fimage[i, j] = Color.FromArgb(
								red: (byte)(fimage[i, j].R + (255 - fimage[i, j].R) * gamma),
								green: (byte)(fimage[i, j].G + (255 - fimage[i, j].G) * gamma),
								blue: (byte)(fimage[i, j].B + (255 - fimage[i, j].B) * gamma)
							);
						}
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Сглаживание изображения (фильтром Гаусса).
		/// </summary>
		/// <param name="strength">Сила сглаживания.</param>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Smooth(int strength = 7)
		{
			var rand = new Random();
			var filter = new double[strength, strength];

			for (int i = 0; i < strength; i++)
			{
				for (int j = 0; j < strength; j++)
				{
					filter[i, j] = rand.NextDouble();
				}
			}

			return this._filtering(filter);
		}

		/// <summary>
		/// Фильтр Собеля.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing Sobel()
		{
			double[,] filterX = new double[3, 3]
			{
				{ -1, 0, 1 },
				{ -2, 0, 2 },
				{ -1, 0, 1 }
			};

			double[,] filterY = new double[3, 3]
			{
				{ -1, -2, -1 },
				{ 0, 0, 0 },
				{ 1, 2, 1 }
			};

			return _filtering2(filterX, filterY);
		}

		/// <summary>
		/// Растяжение гистограммы.
		/// </summary>
		/// <returns>Изображение после применение эффекта.</returns>
		public ImageProccesing StretchHistogram()
		{
			var width = this.Image.Width;
			var height = this.Image.Height;

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				long[] GrHst = new long[256]; long HstValue = 0;
				long[] GrSum = new long[256]; long SumValue = 0;
				for (int row = 0; row < fimage.Bitmap.Height; row++)
					for (int col = 0; col < fimage.Bitmap.Width; col++)
					{
						HstValue = (long)(255 * fimage[row, col].GetBrightness());
						GrHst[HstValue]++;
					}
				for (int level = 0; level < 256; level++)
				{
					SumValue += GrHst[level];
					GrSum[level] = SumValue;
				}
				for (int row = 0; row < fimage.Bitmap.Height; row++)
					for (int col = 0; col < fimage.Bitmap.Width; col++)
					{
						System.Drawing.Color clr = fimage[row, col];
						HstValue = (long)(255 * clr.GetBrightness());
						HstValue = (long)(255f / (fimage.Bitmap.Width * fimage.Bitmap.Height) * GrSum[HstValue] - HstValue);
						int R = (int)Math.Min(255, clr.R + HstValue);

						fimage[row, col] = System.Drawing.Color.FromArgb(Math.Max(R, 0), Math.Max(R, 0), Math.Max(R, 0));
					}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		#endregion Public Methods

		#region Private Methods

		/// <summary>
		/// Фильтрует изображение используя заданный фильтр.
		/// </summary>
		/// <param name="kernel">Фильтр.</param>
		/// <returns>Изображение после применение эффекта.</returns>
		private ImageProccesing _filtering(double[,] kernel)
		{
			/* 
			 * В данном методе используеся операция свертки матрицы.
			 * Каждый из цветовых каналов изображения сворачивается с фильтром.
			 * Подробнее можно почитать: http://habrahabr.ru/post/142818/
			*/
			var width = this.Image.Width;
			var height = this.Image.Height;
			var kernelWidth = kernel.GetLength(0);
			var kernelHeight = kernel.GetLength(1);

			using (var fimage = new FImage(this.Image))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int i = 0; i < width; i++)
				{
					for (int j = 0; j < height; j++)
					{
						double rSum = 0;
						double gSum = 0;
						double bSum = 0;
						double kSum = 0;

						for (int iK = 0; iK < kernelWidth; iK++)
						{
							for (int jK = 0; jK < kernelHeight; jK++)
							{
								int pixelPosX = i + (iK - (kernelWidth / 2));
								int pixelPosY = j + (jK - (kernelWidth / 2));

								if ((pixelPosX < 0) || (pixelPosX >= width)
									|| (pixelPosY < 0) || (pixelPosY >= height))
									continue;

								double kernelVal = kernel[iK, jK];

								rSum += fimage[pixelPosX, pixelPosY].R * kernelVal;
								gSum += fimage[pixelPosX, pixelPosY].G * kernelVal;
								bSum += fimage[pixelPosX, pixelPosY].B * kernelVal;

								kSum += kernelVal;
							}
						}

						kSum = kSum <= 0 ? 1 : kSum;

						rSum /= kSum;
						if (rSum < 0) rSum = 0;
						if (rSum > 255) rSum = 255;

						gSum /= kSum;
						if (gSum < 0) gSum = 0;
						if (gSum > 255) gSum = 255;

						bSum /= kSum;
						if (bSum < 0) bSum = 0;
						if (bSum > 255) bSum = 255;

						fimage[i, j] = Color.FromArgb(
							red: (byte)rSum,
							green: (byte)gSum,
							blue: (byte)bSum
						);
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;
			}

			return this;
		}

		/// <summary>
		/// Фильтрует изображения используя два фильтра.
		/// </summary>
		/// <param name="kernel1">Горизонтальный фильтр.</param>
		/// <param name="kernel2">Вертикальный фильтр.</param>
		/// <returns>Изображение после применение эффекта.</returns>
		private ImageProccesing _filtering2(double[,] kernel1, double[,] kernel2)
		{
			/*
			 * Метод данной фильтрации очень схож с одним фильтром.
			 * Исключение составляет два прохода разных фильтров.
			 * Один проходит изображение по вертикали, другой по горизонтали.
			*/
			var width = this.Image.Width;
			var height = this.Image.Height;

			var newBitmap = new Bitmap(this.Image.Width, this.Image.Height, this.Image.PixelFormat);

			using (var fimage = new FImage(this.Image))
			using (var fimageNew = new FImage(newBitmap))
			{
				var sw = new Stopwatch();
				sw.Start();

				for (int x = 1; x < fimage.Bitmap.Width - 1; x++)
				{
					for (int y = 1; y < fimage.Bitmap.Height - 1; y++)
					{
						double sumX = 0, sumY = 0, sum = 0;

						for (int i = -1; i < kernel1.GetLength(0) - 1; i++)
						{
							for (int j = -1; j < kernel1.GetLength(1) - 1; j++)
							{
								sumX += fimage[x + i, y + j].R * kernel1[i + 1, j + 1];
								sumY += fimage[x + i, y + j].R * kernel2[i + 1, j + 1];
							}
						}

						sum = Math.Sqrt(sumX * sumX + sumY * sumY);
						sum = sum > 255 ? 255 : sum < 0 ? 0 : sum;

						fimageNew[x, y] = Color.FromArgb((byte)sum, (byte)sum, (byte)sum);
					}
				}

				sw.Stop();
				this.LastTimeOperation = sw.Elapsed;

				this.Image = (Bitmap)fimageNew.Bitmap.Clone();
			}

			return this;
		}

		/// <summary>
		/// Вычисление дисперсии велечины.
		/// </summary>
		/// <returns>Дисперсия.</returns>
		/// <param name="x">Значения величины.</param>
		/// <param name="exp">Мат ожидание данной велечины.</param>
		private double getDispersion(double[] x, double exp)
		{
			/*
			 * Вычисление дисперсии по формуле: D(X) = M(X^2) - M(X)^2.
			*/
			var expectedValue = this.getExpectedValue(x.Select(e => e * e).ToArray());

			return expectedValue - exp * exp;
		}

		/// <summary>
		/// Вычисление математического ожидания заданной величины.
		/// </summary>
		/// <param name="x">Значения величины.</param>
		/// <returns>Математическое ожидание.</returns>
		private double getExpectedValue(double[] x)
		{
			/*
			 * Вероятности для вычисление получаются следующим способом:
			 * для каждого различного значение берется его частота встречи в наборе данных
			 * далее частота делится на общее кол-во. Таким образом получаем частоту встречаемости
			 * данного значение в общем наборе.
			 * Для вычисление мат. ожидания нужно просумировать все велечины умноженные на их частоту встречи.
			*/
			var dict = new Dictionary<double, int>();
			foreach (var val in x)
				if (dict.ContainsKey(val)) dict[val]++;
				else dict.Add(val, 1);

			return x.Select(e => e * (dict[e] / (double)x.Length)).Sum();
		}

		/// <summary>
		/// Получение черной-белой сигнатуры изображения.
		/// </summary>
		/// <returns>Сигнатура изображения.</returns>
		private double[] getGrayScaleSignature()
		{
			/*
			 * Вычисление 128 шаговой сигнатуры изображения.
			 * Сигнатура считается для Ч/Б изображение где отсутсвуют каналы R, G, B
			 * и все они равны одному параметру яркости.
			 * Для вычисление 128 шаговой сигнатуры считаем кол-во пикселей каждого значения
			 * яркости ( от 0 до 256 ). Берем каждые 2 соседних значение и считаем для них среднее значение.
			 * Таким образом мы сжимаем промежуток до ( 128 = 256 / 2 ).
			*/
			this.ConvertToGrayScale();

			double[] histoGramImage = new double[256];
			double[] signatureImage = new double[128];

			using (var fimage = new FImage(this.Image))
			{
				for (int x = 0; x < fimage.Bitmap.Width; x++)
					for (int y = 0; y < fimage.Bitmap.Height; y++)
						histoGramImage[fimage[x, y].R]++;

				for (int i = 0; i < 256; i += 2)
					histoGramImage[i] += histoGramImage[i + 1];

				for (int i = 0; i < 128; i++)
					signatureImage[i] = histoGramImage[i * 2];

				signatureImage = signatureImage.Select(x => x * (1 / signatureImage.Sum())).ToArray();
			}

			double test = signatureImage.Sum();
			return signatureImage;
		}

		/// <summary>
		/// Разбивает изображение на сетку 15 на 15 и вычисляет в каждом квадрате дисперсию и мат ожидание.
		/// </summary>
		/// <returns>Массив ячеек сетки с полями: дисперсия и мат ожидание.</returns>
		/// <param name="image">Изображение.</param>
		private InfoGrid[] getInfoGrid(Bitmap image)
		{
			/*
			 * Делит изображение сеткой 15 на 15. Обходит каждую из ячеек
			 * и вычисляет дисперсию и мат ожидание. В качестве величины
			 * берется параметр яркости изображения.
			*/
			var rand = new Random(DateTime.Now.Millisecond);

			List<InfoGrid> listInfoGrid = new List<InfoGrid>();

			int randX, randY;
			for (int x = 0; x < image.Width; x += image.Width / 15 - 1)
			{
				for (int y = 0; y < image.Height; y += image.Height / 15 - 1)
				{
					randX = rand.Next(x, x + image.Width / 15);
					randY = rand.Next(y, y + image.Height / 15);

					if (randX > image.Width - 1 || randY > image.Height - 1 ||
						 randX < 0 || randY < 0)
					{
						x -= x == 0 ? 0 : 1;
						y -= y == 0 ? 0 : 1;

						continue;
					}

					var rectVal = this.getRectVal(randX, randY);
					var expect = this.getExpectedValue(rectVal);
					var disp = this.getDispersion(rectVal, expect);

					listInfoGrid.Add(new InfoGrid(randX, randY, expect, disp));
				}
			}

			return listInfoGrid.ToArray();
		}

		/// <summary>
		/// Обходит квадрат 25 на 25 с центром в x, y преобразовывая каждый пиксель в Lab.
		/// </summary>
		/// <returns>Набор компонент L из пространства Lab.</returns>
		/// <param name="centerX">Центр квадрта по X.</param>
		/// <param name="centerY">Центр квадрата по Y.</param>
		private double[] getRectVal(int centerX, int centerY)
		{
			List<double> pixelVal = new List<double>();

			using (var fimage = new FImage(this.Image))
				for (int i = centerX - 12; i < centerX + 12; i++)
					for (int j = centerY - 12; j < centerY + 12; j++)
					{
						var ii = i - (centerX - 12);
						var jj = j - (centerY - 12);

						if (i < 0 || j < 0 ||
							ii < 0 || jj < 0 ||
							i > this.Image.Width - 1 || j > this.Image.Height - 1 ||
							ii > this.Image.Width - 1 || jj > this.Image.Height - 1)
							continue;

						pixelVal.Add(new Rgb() { R = fimage[i, j].R, G = fimage[i, j].G, B = fimage[i, j].B }.To<Lab>().L);
					}

			return pixelVal.ToArray();
		}

		#endregion Private Methods
	}

	/// <summary>
	/// Аргумент события о изменение прогесса.
	/// </summary>
	public class ProgressEventArgs
	{
		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр.
		/// </summary>
		/// <param name="curPixProccessing">Кол-во пикселей обработаных в данный момент.</param>
		public ProgressEventArgs(long curPixProccessing)
		{
			this.CurPixProccessing = curPixProccessing;
		}

		#endregion Public Constructors

		#region Public Properties

		/// <summary>
		/// Кол-во пикселей обработаных в данный момент.
		/// </summary>
		/// <value>Кол-во пикселей.</value>
		public long CurPixProccessing { get; private set; }

		#endregion Public Properties
	}

	/// <summary>
	/// Структура необходимая для прохода изображения сеткой.
	/// </summary>
	internal class InfoGrid
	{
		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр.
		/// </summary>
		/// <param name="x">Координата x.</param>
		/// <param name="y">Координата y.</param>
		/// <param name="expectedValue">Мат ожидание.</param>
		/// <param name="dispersion">Дисперсия.</param>
		public InfoGrid(int x, int y, double expectedValue, double dispersion)
		{
			this.X = x;
			this.Y = y;
			this.ExpectedValue = expectedValue;
			this.Dispersion = dispersion;
		}

		public InfoGrid()
		{
			this.X = 0;
			this.Y = 0;
			this.ExpectedValue = 0;
			this.Dispersion = 0;
		}

		#endregion Public Constructors

		#region Public Properties

		/// <summary>
		/// Дисперсия.
		/// </summary>
		public double Dispersion { get; set; }

		/// <summary>
		/// Математическое ожидание.
		/// </summary>
		public double ExpectedValue { get; set; }

		/// <summary>
		/// Координата X.
		/// </summary>
		public int X { get; set; }

		/// <summary>
		/// Координата Y.
		/// </summary>
		public int Y { get; set; }

		#endregion Public Properties
	}
>>>>>>> 9fc2a875e7471167c467a33ac21f14e29b256585
}