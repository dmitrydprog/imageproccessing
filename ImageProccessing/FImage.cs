﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace ImageProccessing.FastImage
{
	/// <summary>
	/// Класс для быстрой работы с пикселями.
	/// </summary>
	public class FImage : IDisposable
	{
		#region Private Fields

		private Bitmap _bmp;
		private BitmapData _bmpData;
		private int _bps = 0;
		private int _height;
		unsafe private byte* _imagePointer;
		private int _width;
		private bool disposed = false;
		private SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

		#endregion Private Fields

		#region Public Constructors

		/// <summary>
		/// Создает новый экземпляр из существующий картинки.
		/// Блочит участок памяти от перезаписи до момента пока не будет уничтожен.
		/// </summary>
		/// <param name="image">Изображение.</param>
		public FImage(Bitmap image)
		{
			this._width = image.Width;
			this._height = image.Height;
			this._bmp = image;
			this._bps = Image.GetPixelFormatSize(this._bmp.PixelFormat);

			this._lockBits();
		}

		#endregion Public Constructors

		#region Public Properties

		/// <summary>
		/// Получить текущие изображение.
		/// </summary>
		/// <value>Изображение.</value>
		public Bitmap Bitmap
		{
			get
			{
				return this._bmp;
			}
		}

		#endregion Public Properties

		#region Public Indexers

		/// <summary>
		/// Получить или установить цвет пикселя в координатах x;y.
		/// </summary>
		/// <param name="x">Координата x.</param>
		/// <param name="y">Координата y.</param>
		public Color this[int x, int y]
		{
			get
			{
				unsafe
				{
					byte* ptr = (this._imagePointer + y * this._bmpData.Stride + x * (byte)(this._bps / 8.0));

					return Color.FromArgb(
						blue: *ptr,
						green: *(ptr + 1),
						red: *(ptr + 2)
					);
				}
			}

			set
			{
				unsafe
				{
					byte* ptr = (this._imagePointer + y * this._bmpData.Stride + x * (byte)(this._bps / 8.0));

					*ptr = value.B;
					*(ptr + 1) = value.G;
					*(ptr + 2) = value.R;
				}
			}
		}

		#endregion Public Indexers

		#region Public Methods

		/// <summary>
		/// Освобождает память.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="ImageProccessing.FastImage.FImage"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="ImageProccessing.FastImage.FImage"/> in an unusable state.
		/// After calling <see cref="Dispose"/>, you must release all references to the
		/// <see cref="ImageProccessing.FastImage.FImage"/> so the garbage collector can reclaim the memory that the
		/// <see cref="ImageProccessing.FastImage.FImage"/> was occupying.</remarks>
		public void Dispose()
		{
			this._unlockBits();
			this.Dispose(true);
			GC.SuppressFinalize(this);
			return;
		}

		#endregion Public Methods

		#region Protected Methods

		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
			{
				return;
			}

			if (disposing)
			{
				handle.Dispose();
			}

			disposed = true;
		}

		#endregion Protected Methods

		#region Private Methods

		private void _lockBits()
		{
			this._bmpData = this._bmp.LockBits(
				rect: new Rectangle(0, 0, this._width, this._height),
				flags: ImageLockMode.ReadWrite,
				format: this._bmp.PixelFormat
			);

			unsafe
			{
				this._imagePointer = (byte*)this._bmpData.Scan0.ToPointer();
			}
		}

		private void _unlockBits()
		{
			this._bmp.UnlockBits(this._bmpData);
		}

		#endregion Private Methods
	}
}