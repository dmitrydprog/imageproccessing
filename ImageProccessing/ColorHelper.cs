﻿namespace ImageProccessing
{
	public static class ColorHelper
	{
		#region Public Methods

		public static int MulDiv(int number, int numerator, int denominator)
		{
			return (int)(((long)number * numerator) / denominator);
		}

		#endregion Public Methods
	}
}